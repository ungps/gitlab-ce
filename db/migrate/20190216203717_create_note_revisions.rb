class CreateNoteRevisions < ActiveRecord::Migration[5.0]
  DOWNTIME = false

  def change
    create_table :note_revisions, id: :bigserial do |t|
      t.integer :note_id
      t.text :note_raw
      t.text :note_html
      t.integer :user_id
      t.text :old_diff
      t.text :new_diff

      t.timestamps
    end
  end
end

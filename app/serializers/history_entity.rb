# frozen_string_literal: true

class HistoryEntity < Grape::Entity
  include RequestAwareEntity

  expose :created_at
  expose :note_raw
  expose :note_html
  expose :old_diff
  expose :new_diff#, using: HistoryDiffEntity
  expose :user, using: UserEntity
  # expose :user, using: NoteUserEntity
end

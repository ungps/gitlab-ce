# frozen_string_literal: true

class HistorySerializer < BaseSerializer
  entity HistoryEntity
end

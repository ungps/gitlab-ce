import $ from 'jquery';
import Vue from 'vue';
import Translate from '~/vue_shared/translate';
import SeeHistoryModalWrapper from './see_history_modal_wrapper.vue';

export default function initHistoryTriggers() {
  const seeHistoryModalWrapperEl = document.querySelector('.js-see-history-modal-wrapper');

  if (seeHistoryModalWrapperEl) {
    Vue.use(Translate);

    // eslint-disable-next-line no-new
    new Vue({
      el: seeHistoryModalWrapperEl,
      data() {
        return {
        };
      },
      render(createElement) {
        return createElement(SeeHistoryModalWrapper);
      },
    });
  }
}

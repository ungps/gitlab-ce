# frozen_string_literal: true

module Notes
  class UpdateService < BaseService
    include DiffHelper

    def execute(note)
      return note unless note.editable?

      # puts "hello, I'm editing #{note.inspect}"

      old_mentioned_users = note.mentioned_users.to_a

      old_note = note.note
      old_note_html = note.note_html

      note.update(params.merge(updated_by: current_user))


      old_diff, new_diff = mark_inline_diffs(old_note, note.note)
      # puts "--"
      # puts new_diff


      diffy = Diffy::Diff.new(old_note, note.note, diff: '-U 3', include_diff_info: true)
      # puts "--"
      # puts "#{diffy.diff}"
      # puts "--"
      # diff_lines = diffy.diff
      diff_lines = diffy.diff.scan(/.*\n/)[2..-1]
      # puts "--"
      # puts "#{diff_lines}"
      # puts "--"

      # puts Gitlab::Diff::InlineDiffMarker.new(diff_lines).mark(diff_lines);
      diff_lines = Gitlab::Diff::Parser.new.parse(diff_lines)
      diff_lines = Gitlab::Diff::Highlight.new(diff_lines).highlight
      # puts "----"
      # puts "#{diff_lines}"
      # puts "----"
      # diff_lines.each do |line|
        # puts "#{line.inspect}"
      # end
      # puts "----"


      # diff_lines.each do |line|
        # lines.push(ERB::Util.html_escape(line.rich_text))
      # end

      new_diff = diff_lines;


      # diffy = Diffy::Diff.new(@blob.data, @content, diff: '-U 3', include_diff_info: true)
      # new_diff = Diffy::Diff.new(old_note, note.note, diff: '-U 3', include_diff_info: true)
      # new_diff = new_diff.diff.scan(/.*\n/)[2..-1]
      # new_diff = Gitlab::Diff::Parser.new.parse(new_diff)
      # new_diff = Gitlab::Diff::Highlight.new(new_diff).highlight

      # diffy = Diffy::Diff.new(@blob.data, @content, diff: '-U 3', include_diff_info: true)
      # diff_lines = diffy.diff.scan(/.*\n/)[2..-1]
      # diff_lines = Gitlab::Diff::Parser.new.parse(diff_lines)
      # @diff_lines = Gitlab::Diff::Highlight.new(diff_lines, repository: @repository).highlight


      NoteRevision.create(
        note: note,
        note_raw: old_note,
        note_html: old_note_html,
        user: current_user,
        old_diff: old_diff,
        new_diff: new_diff
      )

      note.create_new_cross_references!(current_user)

      # puts Diffy::Diff.new(old_note_html, note.note_html)

      if note.previous_changes.include?('note')
        TodoService.new.update_note(note, current_user, old_mentioned_users)
      end

      if note.supports_suggestion?
        Suggestion.transaction do
          note.suggestions.delete_all
          Suggestions::CreateService.new(note).execute
        end

        # We need to refresh the previous suggestions call cache
        # in order to get the new records.
        note.reload
      end

      note
    end
  end
end
